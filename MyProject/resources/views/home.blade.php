@extends('layouts.app1')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{$donasi->count()}}<sup style="font-size: 20px"> Unit</sup></h3>

                            <p>Donasi Terkumpul</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-money"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{$donatur->count()}}<sup style="font-size: 20px"> Orang</sup></h3>
                            <p>Donatur</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{$databencana->count()}}</h3>
                            <p>Daerah Bencana</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-location-arrow"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$distribusi->count()}}<sup style="font-size: 20px"> Unit</sup></h3>

                            <p>Donasi Tersalurkan</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-6">
                    <!-- LINE CHART -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Rekap Donasi 2018</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="chart">
                                <canvas id="graph" style="height:250px"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                <!-- right col -->
            </div>

            <!-- /.row (main row) -->
                {{--<canvas id="graph" width="400" height="400"></canvas>--}}
                {{--{{$datas[12]}}--}}
                {{--@foreach($datas as $data)--}}
                    {{--@if($datas[10])--}}
                        {{--10--}}
                    {{--@endif--}}
                    {{--@if($datas[12])--}}
                        {{--12--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
    <script>
        var data = {
            labels: ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],

            datasets: [
                {
                    fillColor: "rgba(100,220,220,0.7)",
                    strokeColor: "rgba(220,220,220,1)",
                    pointColor: "rgba(220,220,220,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [ {{$datas["01"]->count()}},{{$datas["02"]->count()}},{{$datas["03"]->count()}},{{$datas["04"]->count()}},{{$datas["05"]->count()}},{{$datas["06"]->count()}},
                        {{$datas["07"]->count()}},{{$datas["08"]->count()}},{{$datas["09"]->count()}},{{$datas[10]->count()}},{{$datas[11]->count()}},{{$datas[12]->count()}}]
                }
            ]
        };

        var context = document.querySelector('#graph').getContext('2d');

        new Chart(context).Line(data);
    </script>
@endsection
