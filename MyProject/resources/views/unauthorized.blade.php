<div class=”title m-b-md”>
    You cannot access this page! This is for only ‘{{$role}}’”
    <br/>
    Go Redirect <a href="{{url('/')}}" class="btn btn-primary">Here</a>
</div>