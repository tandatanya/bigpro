@extends('layouts.app1')
@section('title')
    <title>Donate|Data Petugas</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Petugas
                <small>Data Petugas</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url('/home')}}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="{{url('/datapetugas')}}"><i class="fa fa-user"></i> Petugas</a></li>
                <li class="active">Data Petugas</li>
            </ol>
        </section>
    <?php
    $number=1;
    ?>

    <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Tabel Data Petugas</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Nomor HP</th>
                                    <th>Alamat</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($datapetugas as $row)
                                    <tr>
                                        <td>{{$number}}</td>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->nomor_handphone}}</td>
                                        <td>{{$row->alamat}}</td>
                                        <td>
                                            @if($row->status=='0')
                                                <a href="#" class="btn btn-sm btn-warning">Belum Terverifikasi</a>
                                                @endif
                                            @if($row->status=='1')
                                                <a href="#" class="btn btn-sm btn-success">Terverifikasi</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{action('DatapetugasController@edit',$row['id'])}}" class="btn btn-info">Edit</a>

                                            <form method="post" class="delete_form" action="{{action('DatadonaturController@destroy', $row['id'])}}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php
                                    $number++;
                                    ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <!-- /.Left col -->
                <!-- right col (We are only adding the ID to make the widgets sortable)-->
                <section class="col-lg-5 connectedSortable">

                </section>
                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
    </div>

@endsection