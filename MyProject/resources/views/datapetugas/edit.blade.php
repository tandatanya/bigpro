<?php $number=1; ?>
@extends('layouts.app1')
@section('title')
 <title>Donate|Data Donasi</title>
@endsection


@section('content')

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <h1>
    Donasi
    <small>Penjemputan Data Donasi</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dahsboard</a></li>
    <li><a href="#"><i class="fa fa-database"></i>Donasi </a></li>
    <li class="active">Data Donasi</li>
   </ol>
  </section>

  <section class="content">
   <div class="row">
    <div class="col-md-12">
     <br />
     <h3>Penjemputan Donasi</h3>
     <br />
     @if(count($errors) > 0)

      <div class="alert alert-danger">
       <ul>
        @foreach($errors->all() as $error)
         <li>{{$error}}</li>
        @endforeach
       </ul>
       @endif


       <form method="post" action="{{action('DatapetugasController@update', $datapetugas->id)}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PATCH" />
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('ID')}}</label>
         <input type="text" name="id" class="form-control" value="{{$datapetugas->id}}" placeholder="Masukkan ID" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Nama')}}</label>
         <input type="text" name="name" class="form-control" value="{{$datapetugas->name}}" placeholder="Masukkan Nama Petugas" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Nomor Telepon')}}</label>
         <input type="text" name="no_hp" class="form-control" value="{{$datapetugas->nomor_handphone}}" placeholder="Masukkan Jadwal Bekerja" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Alamat')}}</label>
         <textarea name="alamat" class="form-control" value="{{$datapetugas->alamat}}" placeholder="Masukkan Alamat Bertugas">{{$datapetugas->alamat}}</textarea>
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Email')}}</label>
         <input type="text" name="email" class="form-control" value="{{$datapetugas->email}}" placeholder="Masukkan Kendaraan Yang Dipakai" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Hash Password')}}</label>
         <input type="text" name="password" class="form-control" value="{{$datapetugas->password}}" placeholder="Masukkan Job" />
        </div>

        <div class="form-group row">
         <label for="status" class="col-md-4 col-form-label text-md-right">{{__('Status User')}}</label>
         <div class="col-md-6">
          <select class="form-control" name="status" id="status">
           @if($datapetugas->status=='0')
            <option value="0" selected>Belum Terverifikasi</option>
            <option value="1">Terverifikasi</option>
            @endif
            @if($datapetugas->status=='1')
            <option value="0" >Belum Terverifikasi</option>
            <option value="1" selected>Terverifikasi</option>
           @endif


          </select>
         </div>
        </div>
        <div class="form-group">
         <input type="submit" class="btn btn-primary" value="Edit" />
        </div>
       </form>
      </div>
    </div>
   </div>
   <!-- /.row -->
   <!-- Main row -->
   <!-- /.row (main row) -->
  </section>
 </div>
@endsection