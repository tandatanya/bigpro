<?php $number=1; ?>
@extends('layouts.app1')
@section('title')
    <title>Donate|Data Donasi</title>
@endsection

@section('head')
 <!--DataTables-->
 <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('content')

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <h1>
    Donasi
    <small>Rekap Data Donasi</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dahsboard</a></li>
    <li><a href="#"><i class="fa fa-database"></i>Donasi </a></li>
    <li class="active">Data Donasi</li>
   </ol>
  </section>

  <section class="content">
   <div class="row">
    <div class="col-xs-12">
     <div class="box">
      <div class="box-header">
       <h3 class="box-title">Tabel Data Donasi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
       <table id="example1" class="table table-bordered table-hover">
        <thead>
        <tr>
         <th>No</th>
         <th>Donatur</th>
         <th>Lokasi</th>
         <th>Jenis Barang</th>
         <th>Jumlah</th>
         <th>Keterangan</th>
         {{--<th>Status</th>--}}
         <th>Penjemputan</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datadonasi as $row)
         <tr>
          <td>{{$number}}</td>
          <td>{{$row['name']}}</td>
          <td>{{$row['lokasi']}}</td>
          <td>{{$row['jenis_barang']}}</td>
          <td>{{$row['jumlah']}}</td>
          <td>{{$row['keterangan']}}</td>
          {{--<td>--}}
           {{--<button class="btn btn-warning">Sedang dijemput</button>--}}
          {{--</td>--}}
          <td>
           <a href="{{action('DatadonasiController@edit',$row['id'])}}" class="btn btn-info">Penjemputan</a>
          </td>
          {{--<td>{{$row['donasi']}}</td>--}}

          {{--<td><a href="{{action('DatadonasiController@edit', $row['id'])}}" class="btn btn-warning">Edit</a></td>--}}
          {{--<td>--}}
          {{--<form method="post" class="delete_form" action="{{action('DatadonasiController@destroy', $row['id'])}}">--}}
          {{--{{csrf_field()}}--}}
          {{--<input type="hidden" name="_method" value="DELETE" />--}}
          {{--<button type="submit" class="btn btn-danger">Hapus</button>--}}
          {{--</form>--}}
          {{--</td>--}}
         </tr>
         <?php $number++; ?>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
         <th>No</th>
         <th>Donatur</th>
         <th>Lokasi</th>
         <th>Jenis Barang</th>
         <th>Jumlah</th>
         <th>Keterangan</th>
         {{--<th>Status</th>--}}
         <th>Penjemputan</th>
        </tr>
        </tfoot>
       </table>
      </div>
      <!-- /.box-body -->
     </div>
     <!-- /.box -->
    </div>
    <!-- /.col -->
   </div>
   <!-- /.row -->
   <!-- Main row -->
  </section>
  <!-- DataTables -->
  <script src="{{asset('../../../public/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('../../../public/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script>
      $(function () {
          $('#example1').DataTable()
          $('#example2').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
          })
      })
  </script>
  {{--<section class="content-header">--}}
   {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
     {{--<br />--}}
     {{--<h3 align="center">Data Donasi yang Telah Masuk</h3>--}}
     {{--<br />--}}
     {{--@if($message = Session::get('success'))--}}
      {{--<div class="alert alert-success">--}}
       {{--<p>{{$message}}</p>--}}
      {{--</div>--}}
     {{--@endif--}}
     {{--<div align="right">--}}
      {{--<a href="{{route('databencana.create')}}" class="btn btn-primary">Tambah</a>--}}
      {{--<br />--}}
      {{--<br />--}}
     {{--</div>--}}

     {{--<table class="table table-bordered table-striped">--}}
      {{--<tr>--}}
       {{--<th>Kode Donasi</th>--}}
       {{--<th>Jenis Barang</th>--}}
       {{--<th>Donasi</th>--}}
       {{--<th>Jumlah</th>--}}
       {{--<th>Keterangan</th>--}}
       {{--<th>Lokasi</th>--}}
       {{--<th>Edit</th>--}}
       {{--<th>Hapus</th>--}}
      {{--</tr>--}}
      {{--@foreach($datadonasi as $row)--}}
       {{--<tr>--}}
        {{--<td>{{$row['kode_donasi']}}</td>--}}
        {{--<td>{{$row['jenis_barang']}}</td>--}}
        {{--<td>{{$row['donasi']}}</td>--}}
        {{--<td>{{$row['jumlah']}}</td>--}}
        {{--<td>{{$row['keterangan']}}</td>--}}
        {{--<td>{{$row['lokasi']}}</td>--}}
        {{--<td><a href="{{action('DatadonasiController@edit', $row['id'])}}" class="btn btn-warning">Edit</a></td>--}}
        {{--<td>--}}
        {{--<form method="post" class="delete_form" action="{{action('DatadonasiController@destroy', $row['id'])}}">--}}
        {{--{{csrf_field()}}--}}
        {{--<input type="hidden" name="_method" value="DELETE" />--}}
        {{--<button type="submit" class="btn btn-danger">Hapus</button>--}}
        {{--</form>--}}
        {{--</td>--}}
       {{--</tr>--}}
      {{--@endforeach--}}
     {{--</table>--}}
    {{--</div>--}}
   {{--</div>--}}
   {{--<script>--}}
       {{--$(document).ready(function(){--}}
           {{--$('.delete_form').on('submit', function(){--}}
               {{--if(confirm("Are you sure you want to delete it?"))--}}
               {{--{--}}
                   {{--return true;--}}
               {{--}--}}
               {{--else--}}
               {{--{--}}
                   {{--return false;--}}
               {{--}--}}
           {{--});--}}
       {{--});--}}
   {{--</script>--}}
  {{--</section>--}}
 {{--</div>--}}
 </div>
@endsection

