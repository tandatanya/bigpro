@extends('layouts.app1')
@section('title')
    <title>Donate | Data Donasi</title>
@endsection

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Donasi
                <small>Rekap Data Donasi</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Dahsboard</a></li>
                <li><a href="#"><i class="fa fa-database"></i>Donasi </a></li>
                <li class="active">Data Donasi</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Tabel Rekap Data Donasi</h3>
                        </div>
                        @if(count($errors) > 0)

                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Donasi</th>
                                    <th>Lokasi</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th>Opsi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $number = 1;
                                ?>
                                @foreach($datadonasi as $row)
                                    <tr>
                                        <td>{{$number}}</td>
                                        <td>{{$row->jenis_barang}}</td>
                                        <td>{{$row->lokasi}}</td>
                                        <td>{{$row->tanggal}}</td>
                                        <td>
                                            @if($row->status=='0')
                                                <a href="#" class="btn btn-danger">Belum Dijemput</a>
                                            @endif
                                            @if($row->status=='1')
                                                <a href="#" class="btn btn-warning">Sedang Dijemput</a>
                                            @endif
                                            @if($row->status=='2')
                                                <a href="#" class="btn btn-primary">Sudah Dijemput</a>
                                            @endif
                                            @if($row->status=='3')
                                                <a href="#" class="btn btn-success">Sudah Terdistribusi</a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{action('DatadonasiController@jemputan', $row['id'])}}" class="btn btn-info">Ubah</a>
                                        </td>
                                    </tr>

                                    <?php
                                    $number++;
                                    ?>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
    </div>
@endsection