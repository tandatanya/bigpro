<?php $number=1; ?>
@extends('layouts.app1')
@section('title')
 <title>Donate|Data Donasi</title>
@endsection


@section('content')

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <h1>
    Donasi
    <small>Penjemputan Data Donasi</small>
   </h1>
   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dahsboard</a></li>
    <li><a href="#"><i class="fa fa-database"></i>Donasi </a></li>
    <li class="active">Data Donasi</li>
   </ol>
  </section>

  <section class="content-header">
   <div class="row">
    <div class="col-md-12">
     <br />
     <h3>Edit Data</h3>
     <br />
     @if(count($errors) > 0)
      <div class="alert alert-danger">
       <ul>
        @foreach($errors->all() as $error)
         <li>{{$error}}</li>
        @endforeach
       </ul>
       @endif
      </div>
      <form method="post" action="{{action('DatadonasiController@update', $datadonasi->id)}}" enctype="multipart/form-data">
       {{csrf_field()}}
       <input type="hidden" name="_method" value="PATCH" />
       <div class="form-group">
        <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Kode Donasi')}}</label>
        <input type="text" name="kode_donasi" class="form-control" value="{{$datadonasi->id}}" placeholder="Masukkan Kode Donasi" />
       </div>
       <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Jenis Barang')}}</label>
         <input type="text" name="jenis_barang" class="form-control" value="{{$datadonasi->jenis_barang}}" placeholder="Masukkan Jenis Barang" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Donasi')}}</label>
         <input type="text" name="donasi" class="form-control" value="{{$datadonasi->donasi}}" placeholder="Masukkan Donasi" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Jumlah')}}</label>
         <input type="text" name="jumlah" class="form-control" value="{{$datadonasi->jumlah}}" placeholder="Masukkan Jumlah Donasi" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Keterangan')}}</label>
         <input type="text" name="keterangan" class="form-control" value="{{$datadonasi->keterangan}}" placeholder="Masukkan Keterangan" />
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Lokasi')}}</label>
         <input type="text" name="lokasi" class="form-control" value="{{$datadonasi->lokasi}}" placeholder="Masukkan Lokasi" />
        </div>
        <div class="form-group row">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Status Donasi')}}</label>
         <div class="col-md-6">
          <select class="form-control" name="status" id="status">
           <option value="0">Belum dijemput</option>
           <option value="1">Sedang dijemput</option>
           <option value="2">Sudah dijemput</option>
           <option value="3">Terdistribusi</option>
          </select>
         </div>
        </div>
        <div class="form-group">
         <label for="type" class="col-md-4 col-form-label text-md-right">{{__('Tanggal Penjemputan')}}</label>
         <input type="date" name="tanggal" class="form-control"  placeholder="Masukkan Tanggal" />
        </div>

        <div class="form-group">
         <input type="submit" class="btn btn-primary" value="Edit" />
        </div>
       </form>
      </div>
    </div>
  </section>
   </div>
@endsection
