@extends('master')
@section('title')
    <title>Donate | Data Bencana</title>
@endsection

@section('content')

    <!-- Start About area -->
    <div id="about" class="about-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Rekap Donasi Anda</h2>
                    </div>
                </div>
            </div>

            @if(count($errors) > 0)

                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <h5 align="center"><b>Donasi Yang Masuk</b></h5>
                <table align="center" width="50%" border="2">
                    <tr>
                        <td align="center"><b>No</b></td>
                        <td align="center"><b>Jenis Donasi</b></td>
                        <td align="center"><b>Lokasi Bencana</b></td>
                        <td align="center"><b>Update Tanggal</b></td>
                        <td align="center"><b>Status Terakhir </b></td>
                    </tr>
                    <?php
                    $number = 1;
                    ?>
                    @foreach($userAddresses as $row)
                        <tr>
                            <td>{{$number}}</td>
                            <td>{{$row->jenis_barang}}</td>
                            <td>{{$row->lokasi}}</td>
                            <td>{{$row->tanggal}}</td>
                            <td>
                                @if($row->status=='0')
                                    <a href="#" class="btn btn-danger">Belum Dijemput</a>
                                @endif
                                @if($row->status=='1')
                                    <a href="#" class="btn btn-warning">Sedang Dijemput</a>
                                @endif
                                    @if($row->status=='2')
                                        <a href="#" class="btn btn-primary">Sudah Dijemput</a>
                                    @endif
                                    @if($row->status=='3')
                                        <a href="#" class="btn btn-success">Sudah Terdistribusi</a>
                                    @endif
                            </td>
                        </tr>

                        <?php
                        $number++;
                        ?>
                    @endforeach

                </table>
                <!-- End col-->
            </div>
        </div>
    </div>
    @endsection