@extends('master')

@section('title')
<title>Donate | Jemput Bantuan Bencana</title>
@endsection

@section('nav')
    <li>
        <a href="#team">Our Team</a>
    </li>
    <li>
        <a href="#tentang">Location</a>
    </li>
    @endsection

@section('content')
<body data-spy="scroll" data-target="#navbar-example">

<?php $no = 1; ?>
    <!-- Start Slider Area -->
    <div id="home" class="slider-area">
        <div class="bend niceties preview-2">
            <div id="ensign-nivoslider" class="slides">
                @foreach($databencana as $row)
                <img src="/images/{{ $row->image }}" alt="" title="#{{$row->id}}" />
                    @endforeach
            </div>

        <!-- direction 1 -->
            @foreach($databencana as $row)
            <div id="{{$row->id}}" class="slider-direction slider-one">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="slider-content">
                            <!-- layer 1 -->
                                <div class="layer-1-2 hidden-xs wow slideInDown" data-wow-duration="2s" data-wow-delay=".2s">
                                    <h1 class="title2" href="{{ url('databencana/post/'.$row->id) }}">{{$row->title}}</h1>
                                </div>
                            <!-- layer 2 -->
                                <div class="layer-1-1 wow slideInUp" data-wow-duration="2s" data-wow-delay=".1s">
                                    <h2 class="title1">{{$row->highligt}}</h2>
                                </div>
                            <!-- layer 3 -->
                                <div class="layer-1-3 hidden-xs wow slideInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                    <a class="ready-btn page-scroll" href="{{ url('databencana/post/'.$row->id) }}">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <?php
                    $no++;
                ?>
            @endforeach
        </div>
    </div>
  <!-- End Slider Area -->


        <!-- Start Bencana area -->
        <div id="bencana" class="about-area area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="section-headline text-center">
                            <h2>Daftar Bencana Yang Pernah Terjadi</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>


            @foreach($databencana as $row)
                <div class="about-area area-padding">
            <div class="container">

                <div class="row">
                    <!-- single-well start-->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="well-left">
                            <div class="single-well">
                                <a href="{{ url('databencana/post/'.$row->id) }}">
                                    <img src="/images/{{ $row->image }}" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- single-well end-->
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="well-middle">
                            <div class="single-well">
                                <a href="{{ url('databencana/post/'.$row->id) }}">
                                    <h4 class="sec-head">{{$row['title']}}</h4>
                                </a>
                                <p>
                                    {{$row['deskripsi_bencana']}}
                                </p>
                                <ul>
                                    <li>
                                        <i class="fa fa-check"></i> Pakaian Pria Dewasa
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> Pakaian Wanita Dewasa
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> Handuk
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> Selimut
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End col-->
                </div>
            </div>
                </div>
            @endforeach

    <div id="team" class="our-team-area area-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-headline text-center">
                        <h2>Our special Team</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="team-top">
                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <img src="/img/team/dimas.jpg" alt="">
                            </div>
                            <div class="team-content text-center">
                                <h4>Muhammad Dimas Pratama</h4>
                                <h6>Wibu No Life Sok Edgy</h6>
                            </div>
                        </div>
                    </div>
                    <!-- End column -->
                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <img src="/img/team/dika1.jpg" alt="">
                            </div>
                            <div class="team-content text-center">
                                <h4>Fardika Rais Hidayatullah</h4>
                                <h6>Chubby Panda</h6>
                            </div>
                        </div>
                    </div>
                    <!-- End column -->
                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="single-team-member">
                            <div class="team-img">
                                <img src="/img/team/naufal.jpg" alt="">
                            </div>
                            <div class="team-content text-center">
                                <h4>Naufal Alfiansyah Kurniawan</h4>
                                <h6>Sarkemers</h6>
                            </div>
                        </div>
                    </div>
                    <!-- End column -->
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div id="tentang" class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="footer-content">
                            <div class="footer-head">
                                <div class="footer-logo">
                                    <h2><span>D</span>onate</h2>
                                </div>
                                <p>Mempermudah anda dalam melakukan donasi terhadap korban bencana yang terjadi</p>
                                <div class="footer-icons">
                                    <ul>
                                        <li>
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-google"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="footer-content">
                            <div class="footer-head">
                                <h4>information</h4>
                                <p>Jika membutuhkan informasi lebih lanjut bisa menghubungi kontak yang ada dibawah ini</p>
                                <div class="footer-contacts">
                                    <p><span>Tel:</span> +62 855 4765 0812</p>
                                    <p><span>Email:</span> donate@gmail.com</p>
                                    <p><span>Working Hours:</span> 9am-5pm</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-area-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="copyright text-center">
                            <strong>Copyright &copy; 2018 <a href="#">TandaTanya Corp</a>.</strong> All rights reserved
                        </div>
                        <div class="credits">
                            <b>Matakuliah</b> Pengembangan Aplikasi Berbasis Web
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
@endsection


  <!-- End About area

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  //JavaScript Libraries
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/venobox/venobox.min.js"></script>
  <script src="lib/knob/jquery.knob.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/parallax/parallax.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
  <script src="lib/appear/jquery.appear.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>

  //Contact Form JavaScript File
  <script src="contactform/contactform.js"></script>

  <script src="js/main.js"></script> -->



























<!-- <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        Fonts 
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        Styles
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Coming Soon
                </div>

                <div class="links">
                    <a href="#">Dimas</a>
                    <a href="#">Fardika</a>
                    <a href="#">Naufal</a>
                    <a href="#">Dari</a>
                    <a href="#">Kelompok</a>
                    <a href="#">TandaTanya</a>
                </div>
            </div>
        </div>
    </body>
</html> -->
