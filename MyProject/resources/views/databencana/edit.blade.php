@extends('master')

@section('content')
 <div class="about-area area-padding">s
<div class="row align-items-start">
 <div class="col-sm-8 align-self-center">
 <div class="col-md-12 align-self-center">
  <br />
  <h3>Edit Data</h3>
  <br />
  @if(count($errors) > 0)

  <div class="alert alert-danger">
         <ul>
         @foreach($errors->all() as $error)
          <li>{{$error}}</li>
         @endforeach
         </ul>
  @endif
   <form method="post" action="{{action('DatabencanaController@update', $databencana->id)}}" enctype="multipart/form-data">
    {{csrf_field()}}

    @if ($message = Session::get('success'))
     <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
     </div>
    @endif

    <div class="form-group">
     <input type="text" name="kode_bencana" class="form-control" value="{{$databencana->kode_bencana}}" disabled/>
    </div>
    <div class="form-group">
     <input type="text" name="title" class="form-control" value="{{$databencana->title}}" placeholder="Masukkan Judul Bencana" />
    </div>
    <div class="form-group">
     <input type="text" name="highligt" class="form-control" value="{{$databencana->highligt}}" placeholder="Masukkan Deskripsi Singkat Bencana" />
    </div>
    <div class="form-group">
     <input type="text" name="deskripsi_bencana" class="form-control" value="{{$databencana->deskripsi_bencana}}" placeholder="Masukkan Deskripsi Lengkap Bencana" />
    </div>
    <div class="form-group">
     <strong>Image:</strong>
     <p>{{$databencana->image}}</p>
     {{--<input type="file" name="image" class="form-control" value="{{$databencana->image}}">--}}
     {{--<input type="text" name="highligt" class="form-control" placeholder="Masukkan Deskripsi Singkat Bencana" />--}}
    </div>
    <div class="form-group">
     <input type="text" name="lokasi_bencana" class="form-control" value="{{$databencana->lokasi_bencana}}" placeholder="Masukkan Lokasi Bencana" />
    </div>
    <div class="form-group">
     <input type="submit" class="btn btn-primary" />
    </div>
   </form>
 </div>
</div>
</div>
 </div>
 </div>
@endsection