@extends ('layouts.app1')
<?php $number=1; ?>
@section('title')
    <title>Donate|Data Bencana</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Data Bencana
                <small>Tambah Data Bencana</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Input Data Bencana</a></li>
                <li class="active">Creates</li>
            </ol>
        </section>


        <section class="content-header">
            <div class="row">
                <div class="col-md-12">
                    <br />
                    <h3 aling="center">Tambah Data</h3>
                    <br />
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(\Session::has('success'))
                        <div class="alert alert-success">
                            <p>{{ \Session::get('success') }}</p>
                        </div>
                    @endif
                    <form method="post" action="{{url('databencana')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <div class="form-group">
                            <input type="text" name="kode_bencana" class="form-control" value="BCN" disabled/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" placeholder="Masukkan Judul Bencana" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="highligt" class="form-control" placeholder="Masukkan Deskripsi Singkat Bencana" />
                        </div>
                        <div class="form-group">
                            <input type="text" name="deskripsi_bencana" class="form-control" placeholder="Masukkan Deskripsi Lengkap Bencana" />
                        </div>
                        <div class="form-group">
                            <strong>Image:</strong>
                            <input type="file" name="image" class="form-control">
                            {{--<input type="text" name="highligt" class="form-control" placeholder="Masukkan Deskripsi Singkat Bencana" />--}}
                        </div>
                        <div class="form-group">
                            <input type="text" name="lokasi_bencana" class="form-control" placeholder="Masukkan Lokasi Bencana" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" />
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection