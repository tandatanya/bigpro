@extends('master')
@section('title')
    <title>Donate | Data Bencana</title>
    @endsection

@section('content')

<!-- Start About area -->
<hr>
<div id="about" class="about-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-headline text-center">
                    <h2>Daftar Bencana Yang Pernah Terjadi</h2>
                </div>
            </div>
        </div>

        @if(count($errors) > 0)

            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row">
            <!-- single-well start-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-left">
                    <div class="single-well">
                        <a href="#">
                            <img src="/images/{{ $databencana->image }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <!-- single-well end-->
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="well-middle">
                    <div class="single-well">
                        <a href="#">
                            <h4 class="sec-head">{{$databencana['title']}}</h4>
                        </a>
                        <p>
                            {{$databencana['deskripsi_bencana']}}
                        </p>
                        <a href="{{url('/datadonasi/donasi/'.$databencana['id'])}}" class="btn btn-success">Donasikan</a>
                    </div>
                </div>
            </div>
            <!-- End col-->
        </div>
    </div>
    <br>
    <br>
    <h5 align="center"><b>Donasi Yang Masuk</b></h5>
    <table align="center" width="50%" border="2">
        <tr>
            <td align="center"><b>No</b></td>
            <td align="center"><b>Jenis Donasi</b></td>
            <td align="center"><b>Jumlah</b></td>
            <td align="center"><b>Keterangan</b></td>
        </tr>
        <?php
            $number = 1;
        ?>
            @foreach($datadonasi as $row)
            <tr>
                <td>{{$number}}</td>
                <td>{{$row->jenis_barang}}</td>
                <td>{{$row->jumlah}}</td>
                <td>{{$row->keterangan}}</td>
        </tr>

            <?php
            $number++;
            ?>
            @endforeach

    </table>
</div>