@extends('master')

@section('content')
<div class="row">
 <div class="col-md-12">
  <br />
  <h3 aling="center">Tambah Data</h3>
  <br />
  @if(count($errors) > 0)
  <div class="alert alert-danger">
   <ul>
   @foreach($errors->all() as $error)
    <li>{{$error}}</li>
   @endforeach
   </ul>
  </div>
  @endif
  @if(\Session::has('success'))
  <div class="alert alert-success">
   <p>{{ \Session::get('success') }}</p>
  </div>
  @endif

  <form method="post" action="{{url('datadonatur')}}">
   {{csrf_field()}}
   <div class="form-group">
    <input type="text" name="id" class="form-control" placeholder="Masukkan ID" />
   </div>
   <div class="form-group">
    <input type="text" name="nama_donatur" class="form-control" placeholder="Masukkan Nama Donatur" />
   </div>
   <div class="form-group">
    <input type="text" name="jumlah_donasi" class="form-control" placeholder="Masukkan Jumlah Donasi" />
   </div>
   <div class="form-group">
    <input type="text" name="status" class="form-control" placeholder="Masukkan Status" />
   </div>
   <div class="form-group">
    <input type="text" name="nomor_handphone" class="form-control" placeholder="Masukkan Nomor Handphone" />
   </div>
   <div class="form-group">
    <input type="text" name="alamat" class="form-control" placeholder="Masukkan Alamat" />
   </div>
   <div class="form-group">
    <input type="submit" class="btn btn-primary" />
   </div>
  </form>
 </div>
</div>
@endsection