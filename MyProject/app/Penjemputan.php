<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjemputan extends Model
{
    //
    protected $table = 'penjemputan';
    protected $fillable = ['datadonasi_id','user_id','status', 'tanggal'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function jemputan(){
        return $this->hasMany(Datadonasi::class);
    }
    public function donasi(){
        return $this->belongsTo('App\Datadonasi','datadonasi_id','id');
    }
}
