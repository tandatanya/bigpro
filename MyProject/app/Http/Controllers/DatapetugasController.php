<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Datapetugas;

class DatapetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datapetugas = User::where('type','petugas')->get();
        return view('datapetugas.index', compact('datapetugas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('datapetugas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'id'                 =>  'required',
            'nama_petugas'       =>  'required',
			'jadwal_bekerja'     =>  'required',
			'alamat_bertugas'    =>  'required',
			'kendaraan'          =>  'required',
			'job'                =>  'required'
        ]);
        $datapetugas = new Datapetugas([
            'id'    =>  $request->get('id'),
            'nama_petugas'     =>  $request->get('nama_petugas'),
			'jadwal_bekerja'    =>  $request->get('jadwal_bekerja'),
			'alamat_bertugas'    =>  $request->get('alamat_bertugas'),
			'kendaraan'    =>  $request->get('kendaraan'),
			'job'    =>  $request->get('job')
        ]);
        $datapetugas->save();
        return redirect()->route('datapetugas.create')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $no
     * @return \Illuminate\Http\Response
     */
    public function show($no)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $no
     * @return \Illuminate\Http\Response
     */
    public function edit($no)
    {
       $datapetugas = User::find($no);
       return view('datapetugas.edit', compact('datapetugas', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $no
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $no)
    {
       $this->validate($request, [
            'id'                 =>  'required',
            'name'       =>  'required',
			'no_hp'     =>  'required',
			'alamat'    =>  'required',
			'email'     =>  'required',
			'password'  =>  'required',
            'status'=>'required',

        ]);
        $datapetugas = User::find($no);
        $datapetugas->id = $request->get('id');
        $datapetugas->name = $request->get('name');
		$datapetugas->nomor_handphone = $request->get('no_hp');
		$datapetugas->alamat = $request->get('alamat');
		$datapetugas->email = $request->get('email');
		$datapetugas->status = $request->get('status');
        $datapetugas->save();
        return redirect()->route('datapetugas.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $no
     * @return \Illuminate\Http\Response
     */
    public function destroy($no)
    {
       $datapetugas = Datapetugas::find($no);
       $datapetugas->delete();
       return redirect()->route('datapetugas.index')->with('success', 'Data Deleted');
    }
}
