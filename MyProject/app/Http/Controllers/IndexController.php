<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Databencana;
use App\Penjemputan;

class IndexController extends Controller
{
    public function index()
    {
//       $databencana = Databencana::all()->toArray();
//        $dataslider = Databencana::all()->take( 3)->get();
        $databencana = Databencana::get();
        return view('welcome',compact('databencana'));
    }

    public function distribusi(){
        $userAddresses = Penjemputan::select("penjemputan.*", "datadonasis.*")
            ->join("datadonasis", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->where("penjemputan.status", "3")
            ->get();
//            dd($userAddresses);
        return view('distribusi',compact(['userAddresses']));
    }

    public function regisAdmin(){
        return view('auth.registerAdmin');
    }

    public function postingan(){

    }
}
