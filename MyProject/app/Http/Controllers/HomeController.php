<?php

namespace App\Http\Controllers;
use App\Datadonasi;
use App\User;
use App\Datapetugas;
use App\Penjemputan;
use Illuminate\Http\Response;
use App\Databencana;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin(Request $request){
        return view('middleware')->withMassage("Admin");
    }

    public function petugas(Request $request){
        return view('middleware')->withMessage("Petugas");
    }

    public function member(Request $request){
        return view('middleware')->withMessage("Member");
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $distribusi = Penjemputan::select("penjemputan.*", "datadonasis.*")
            ->join("datadonasis", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->where("penjemputan.status", "3")
            ->get();

        $datas= Datadonasi::select('id', 'jenis_barang','donasi', 'created_at')
            ->get()
            ->groupBy(function($val) {
                return Carbon::parse($val->created_at)->format('m');
            });
//        dd($datas);
        $user_list = Datadonasi::select('id', 'jenis_barang','donasi', 'created_at')
            ->orderBy('created_at')->get();// get User collection
        $user_list->groupBy(function ($item, $key) {
            return $item->created_at->format('m');
        });
//        dd($user_list);

        $databencana = Databencana::all();
        $donasi = Datadonasi::all();
        $donatur = User::all()->where('type','member');

        if ($request->user() && $request->user()->type == 'admin')
        {
            return view('home',compact(['distribusi','donasi','donatur',
                'databencana','datas','user_list']));
        }
        elseif ($request->user() && $request->user()->type == 'petugas')
        {
            if($request->user() && $request->user()->status == '1'){
                return view('home',compact(['distribusi','donasi','donatur',
                    'databencana']));
        }
        else{
                return view('welcome',compact('databencana'))->with('danger','Akun Anda belum terverifikasi');
        }
        }
        else{
            $databencana = Databencana::get();
            return view('welcome',compact('databencana'));
        }
//        return view('home');
    }

    public function rekap($id){
        $userAddresses = Penjemputan::select("penjemputan.*", "datadonasis.*")
            ->join("datadonasis", "penjemputan.datadonasi_id", "=", "datadonasis.id")
            ->where("datadonasis.user_id", $id)
            ->get();
        $penjemputan = User::find($id)->orders();
//        $penjemputan = Penjemputan::find($id)->orders;
//        $penjemputan = Penjemputan::with('datadonasi')
//            ->whereColumn('kode_donasi','datadonasis.id')
//            ->whereColumn('datadonasi.kode_bencana','databencana.id')
//            ->where('kode_donatur',$id)
//            ->get();
//        $datadonasi = Datadonasi::find($penjemputan->datadonasi_id);
        return view('rekapdonasi',compact(['userAddresses']));
    }

//    public function someAdminStuff(Request $request)
//    {
//        $request->user()->authorizeRoles('petugas');
//    }

}
