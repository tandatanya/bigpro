<?php

namespace App\Http\Controllers;
use App\Penjemputan;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class PenjemputanController extends Controller
{
    //
    public function update(Request $request, $id)
    {

        $penjemputan = Penjemputan::find($id);
        $penjemputan->status = $request->status;
        $penjemputan->tanggal = $request->tanggal;
        $penjemputan->save();
        return redirect()->action('PenjemputanController@index');
//        return redirect()->route('datadonasi','distribusi',compact('datadonasi'));
//        return new Response(view('datadonasi.distribusi',compact('datadonasi')));
    }

    public function index(){
        return view('datadonasi.distribut',compact('datadonasi'));

    }
}
