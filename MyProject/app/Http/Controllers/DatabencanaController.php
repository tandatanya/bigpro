<?php

namespace App\Http\Controllers;

use App\Datadonasi;
use Illuminate\Http\Request;

use App\Databencana;
use Symfony\Component\VarDumper\Cloner\Data;

class DatabencanaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//       $databencana = Databencana::all()->toArray();
        $databencana = Databencana::get();
        return view('databencana.index', compact('databencana'));
    }
    public function upload(Request $request)
    {
        $this->validate($request, [
            'kode_bencana'     =>  'required',
            'title'     =>  'required',
            'highligt'   =>  'required',
            'deskripsi_bencana' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'lokasi_bencana' => 'required'

        ]);
        $input['kode_bencana'] = uniqid();
        $input['title'] = $request->title;
        $input['highligt']  = $request->highligt;
        $input['deskripsi_bencana'] = $request->deskripsi_bencana;
        $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $input['image']);
        $input['lokasi_bencana']  = $request->lokasi_bencana;

        Databencana::create($input);
        return redirect()->route('databencana.index');
//        return redirect()->url('/')->with('success', 'Data berhasil ditambahkan');
//
//        $databencana = new Databencana([
//            'kode_bencana'     =>  $request->get('kode_bencana'),
//            'title'     =>  $request->get('title'),
//            'highligt'   =>  $request->get('highligt'),
//            'deskripsi_bencana' =>  $request->get('deskripsi_bencana'),
//            'image' =>  $request->get('image'),
//            'lokasi_bencana'    =>  $request->get('lokasi_bencana')
//        ]);
//        $databencana->save();
//        return redirect()->route('databencana.create')->with('success', 'Data Added');
//        return redirect()->route('databencana.index')->with('success', 'Data Updated');
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('databencana.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
//            'kode_bencana'     =>  'required',
            'title'     =>  'required',
            'highligt'   =>  'required',
            'deskripsi_bencana' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'lokasi_bencana' => 'required'

        ]);
        $input['kode_bencana'] = $request->kode_bencana + rand();
        $input['title'] = $request->title;
        $input['highligt']  = $request->highligt;
        $input['deskripsi_bencana'] = $request->deskripsi_bencana;
        $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $input['image']);
        $input['lokasi_bencana']  = $request->lokasi_bencana;

        Databencana::create($input);
        return redirect()->route('databencana.index')->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kode_bencana)
    {
        //
    }

    public function postingan($id){
        $databencana = Databencana::find($id);
        $datadonasi = Datadonasi::where('databencana_id',$id)->get();
//        $datadonasi = Datadonasi::find('kode_donasi','DNS1');
        return view('databencana.bencana', compact('databencana','datadonasi'));
    }
//
//
//    public function postingan($kode_bencana){
//        $databencana = Databencana::find($kode_bencana)->get();
//        return view('databencana.bencana', compact('databencana'));
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $databencana = Databencana::find($id);
        return view('databencana.edit', compact('databencana', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
//            'kode_bencana'     =>  'required',
            'title'     =>  'required',
            'highligt'   =>  'required',
            'deskripsi_bencana' => 'required',
            //'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'lokasi_bencana' => 'required'

        ]);
        $databencana = Databencana::find($id);
       $input['kode_bencana'] = $request->get('kode_bencana');
        $input['title'] = $request->get('title');
        $input['highligt']  = $request->get('highligt');
        $input['deskripsi_bencana'] = $request->get('deskripsi_bencana');
        $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $input['image']);
        $input['lokasi_bencana']  = $request->get('lokasi_bencana');
        $databencana->save();
        return redirect()->route('databencana.index')->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode_bencana)
    {
		$databencana = Databencana::find($kode_bencana);
        $databencana->delete();
        return redirect()->route('databencana.index')->with('success', 'Data Deleted');
    }

}

