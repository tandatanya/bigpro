<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Datadonatur;

class DatadonaturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datadonatur = User::where('type','member')->get();
//        $datadonatur = User::select("users.*")->where('type','member')->get();
//        $datadonatur = Users::all()->toArray();
        return view('datadonatur.index', compact('datadonatur'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('datadonatur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request, [
            'id'                 =>  'required',
            'nama_donatur'       =>  'required',
			'jumlah_donasi'      =>  'required',
			'status'             =>  'required',
			'nomor_handphone'    =>  'required',
			'alamat'             =>  'required'
        ]);
        $datadonatur = new Datadonatur([
            'id'                 =>  $request->get('id'),
            'nama_donatur'       =>  $request->get('nama_donatur'),
			'jumlah_donasi'      =>  $request->get('jumlah_donasi'),
			'status'             =>  $request->get('status'),
			'nomor_handphone'    =>  $request->get('nomor_handphone'),
			'alamat'             =>  $request->get('alamat')
        ]);
        $datadonatur->save();
        return redirect()->route('datadonatur.create')->with('success', 'Data Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($no)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($no)
    {
        $datadonatur = Datadonatur::find($no);
        return view('datadonatur.edit', compact('datadonatur', 'no'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $no
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $no)
    {
        $this->validate($request, [
            'id'                 =>  'required',
            'nama_donatur'       =>  'required',
			'jumlah_donasi'      =>  'required',
			'status'             =>  'required',
			'nomor_handphone'    =>  'required',
			'alamat'             =>  'required'
        ]);
        $datadonatur = Datadonatur::find($no);
        $datadonatur->id              = $request->get('id');
        $datadonatur->nama_donatur    = $request->get('nama_donatur');
		$datadonatur->jumlah_donasi   = $request->get('jumlah_donasi');
		$datadonatur->status          = $request->get('status');
		$datadonatur->nomor_handphone = $request->get('nomor_handphone');
		$datadonatur->alamat          = $request->get('alamat');
        $datadonatur->save();
        return redirect()->route('datadonatur.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $no
     * @return \Illuminate\Http\Response
     */
    public function destroy($no)
    {
        $datadonatur = User::find($no);
        $datadonatur->delete();
        return redirect()->route('datadonatur.index')->with('success', 'Data Deleted');
    }
}
