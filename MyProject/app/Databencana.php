<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Databencana extends Model
{
    protected $table = 'databencanas';
    protected $fillable = ['kode_bencana','title','highligt', 'deskripsi_bencana','image' ,'lokasi_bencana'];
}
