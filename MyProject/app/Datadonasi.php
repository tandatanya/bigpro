<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datadonasi extends Model
{
    protected $table = 'datadonasis';
    protected $fillable = ['user_id','databencana_id', 'jenis_barang', 'donasi', 'jumlah', 'keterangan', 'lokasi'];

    public function donasis(){
        return $this->belongsTo('App\Penjemputan','datadonasi_id','id');
    }
}
