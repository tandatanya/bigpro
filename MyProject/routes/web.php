<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','IndexController');
Route::get('/dashboard','IndexController@index');
Route::get('/distribusi','IndexController@distribusi');
Route::get('datadonasi/distribusi','DatadonasiController@distribusi');
Route::get('/datadonasi/distribusi/{id}','DatadonasiController@jemputan');
Route::resource('/penjemputan','PenjemputanController');
Route::resource('/datadonatur', 'DatadonaturController');


Route::get('/register/petugas',function(){
    return view('auth.registerAdmin');
});

Route::get('/rekap/{id}','HomeController@rekap');
Route::get('datadonasi/donasi/{id}','DatadonasiController@create');
Route::get('databencana/post/{kode_bencana}', 'DatabencanaController@postingan');


Route::group(['middleware'=> 'App\Http\Middleware\MemberMiddleware'],
    function ()
    {
        Route::resource('datadonasi', 'DatadonasiController');
        Route::match(['get','post'],'/memberOnlyPage/',
            'HomeController@member' );
    });

Route::group(['middleware'=> 'App\Http\Middleware\AdminMiddleware'],
    function ()
    {
        Route::resource('datapetugas', 'DatapetugasController');
        Route::match(['get','post'],'/adminOnlyPage/',
            'HomeController@admin' );
    });

Route::group(['middleware'=> 'App\Http\Middleware\PetugasMiddleware'],
    function ()
    {
        Route::resource('databencana', 'DatabencanaController');
        Route::resource('datapetugas', 'DatapetugasController');
        Route::resource('datadonasi', 'DatadonasiController');
        Route::resource('datadonatur', 'DatadonaturController');
        Route::match(['get','post'],'/petugasOnlyPage/',
            'HomeController@petugas' );
    });
// Route::get('/donasi',function(){
//     return view('datadonasi/index');
// });

//
//Route::get('databencana', 'DatabencanaController@index');
//Route::post('databencana', 'DatabencanaController@upload');
//Route::delete('databencana/{id}', 'DatabencanaController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
