<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatapetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datapetugas', function (Blueprint $table) {
            $table->increments('no');
			$table->string('id');
			$table->string('nama_petugas');
			$table->string('jadwal_bekerja');
			$table->string('alamat_bertugas');
			$table->string('kendaraan');
			$table->string('job');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datapetugas');
    }
}
