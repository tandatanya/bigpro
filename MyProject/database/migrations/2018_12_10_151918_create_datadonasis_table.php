<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatadonasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datadonasis', function (Blueprint $table) {
            $table->increments('id');
			$table->string('user_id');
			$table->string('databencana_id');
			$table->string('jenis_barang');
			$table->string('donasi');
			$table->bigInteger('jumlah');
			$table->string('keterangan');
			$table->string('lokasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datadonasis');
    }
}
